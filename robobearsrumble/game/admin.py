from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Activity)
admin.site.register(ActivityCompletion)
admin.site.register(AchievementCompletion)
admin.site.register(PlayerScore)


class AchievementRequirementInline(admin.TabularInline):
    model = AchievementRequirement
    extra = 1


class AchievementAdmin(admin.ModelAdmin):
    inlines = (AchievementRequirementInline,)


admin.site.register(Achievement, AchievementAdmin)