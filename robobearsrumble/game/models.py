from django.contrib.auth.models import User
from django.db.models import Sum
from django.db import models
from django.contrib.auth import get_user_model

# Model for an activity that players will complete.
from django.db.models.signals import post_save


class Activity(models.Model):
    # Whether the activity is available.
    is_available = models.BooleanField()

    # How many points are scored by finishing the activity
    point_award = models.IntegerField()

    # How long to make the user wait while doing their task
    wait_time = models.IntegerField()

    # Stores the date when the activity was added
    date_published = models.DateTimeField(auto_now_add=True)

    # The GPS coordinates of the activity
    longitude = models.FloatField()
    latitude = models.FloatField()

    # The name of the activity
    name = models.CharField(max_length=256, unique=True)

    code = models.CharField(max_length=32, unique=True)

    description = models.TextField(default='')
    alternative_available = models.BooleanField(default=False)
    alternative_description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class ActivityCompletion(models.Model):
    # Points to the activity that was completed
    activity = models.ForeignKey(Activity, on_delete=models.PROTECT)

    # Points to the player who completed the activity
    player = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='player_activities')

    # When the activity was completed (automatically set)
    completion_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} completed by {} at {}'.format(self.activity, self.player.username, self.completion_time)


class Achievement(models.Model):
    name = models.CharField(max_length=256, unique=True)
    description = models.TextField()

    point_award = models.IntegerField()

    required_number_of_activities = models.IntegerField(default=0)

    required_number_of_points = models.IntegerField(default=0)

    requirements = models.ManyToManyField(Activity, through="AchievementRequirement")

    # The icon (image) to show for this achievement
    icon = models.ImageField(upload_to='icons/')

    def __str__(self):
        return self.name


class AchievementRequirement(models.Model):
    achievement = models.ForeignKey(Achievement, on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)


def check_for_new_achievements(player):
    player_achievement_completions: [AchievementCompletion] = player.player_achievements.all()
    player_achievements = [ac.achievement for ac in player_achievement_completions]

    player_activity_completions: [ActivityCompletion] = player.player_activities.all()
    player_activities = [ac.activity for ac in player_activity_completions]

    all_achievements = Achievement.objects.all()

    completed = []
    for achievement in all_achievements:
        if achievement in player_achievements:
            continue

        all_requirements = achievement.requirements.all()
        if not all(req in player_activities for req in all_requirements):
            continue

        if player.playerscore.score < achievement.required_number_of_points:
            continue

        if len(player_activities) < achievement.required_number_of_activities:
            continue

        completed.append(achievement)

    return completed


class AchievementCompletion(models.Model):
    # Points to the achievement that was completed
    achievement = models.ForeignKey(Achievement, on_delete=models.PROTECT)

    # Points to the player who completed the achievement
    player = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='player_achievements')

    # When the activity was completed (automatically set)
    completion_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} completed by {} at {}'.format(self.achievement, self.player.username, self.completion_time)


class PlayerScore(models.Model):
    player = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)

    score = models.IntegerField(default=0)

    def __str__(self):
        return '{}\'s score: {}'.format(self.player.username, self.score)

    def calculate_player_score(self):
        activities_points = self.player.player_activities.aggregate(Sum('activity__point_award'))['activity__point_award__sum']
        achievements_points = self.player.player_achievements.aggregate(Sum('achievement__point_award'))['achievement__point_award__sum']

        if activities_points is None:
            activities_points = 0
        if achievements_points is None:
            achievements_points = 0

        self.score = achievements_points + activities_points
        self.save()


def update_player_score(sender, **kwargs):
    award = kwargs["instance"]
    player = award.player
    player.playerscore.calculate_player_score()


def create_score(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_score = PlayerScore(player=user)
        user_score.save()


# Create scores when a user is created
post_save.connect(create_score, sender=User)

# Update the relevant player's score whenever an award/achievement is added
post_save.connect(update_player_score, sender=ActivityCompletion)
post_save.connect(update_player_score, sender=AchievementCompletion)
