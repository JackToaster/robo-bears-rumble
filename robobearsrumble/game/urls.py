from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('404', views.four_oh_four, name='four_oh_four'),
    path('achievements', views.achievements, name='achievements'),
    path('leaderboard', views.leaderboard, name='leaderboard'),
    path('map', views.map_view, name='map'),
    path('code/<str:code>', views.code, name='code'),
    path('completed/<int:activity_id>', views.completed, name='completed'),
    path('privacy', views.privacy, name='privacy'),
]
