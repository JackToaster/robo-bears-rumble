# Generated by Django 3.1.5 on 2021-01-17 03:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0007_achievement_requirements'),
    ]

    operations = [
        migrations.AddField(
            model_name='achievement',
            name='required_number_of_activities',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='achievement',
            name='required_number_of_points',
            field=models.IntegerField(default=0),
        ),
    ]
