from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages import get_messages
from django.db.models import Window
from django.db.models.functions import Rank, DenseRank
from django.shortcuts import render, redirect
from django.http import HttpResponse
from datetime import date
from urllib.parse import urlencode

from django.urls import reverse

from .models import Activity, ActivityCompletion, Achievement, PlayerScore, check_for_new_achievements, \
    AchievementCompletion


# Create your views here.
def index(request):
    available_activities = Activity.objects.filter(is_available=True)
    return render(request, 'index.html', {'activities': available_activities})


@login_required()
def profile(request):
    if request.user.is_authenticated:
        return HttpResponse("Logged in successfully")
    else:
        return HttpResponse("not Logged in")


def privacy(request):
    return render(request, 'privacy.html')


@login_required(login_url='/accounts/new_user/')
def code(request, code):
    activity = Activity.objects.filter(code=code).first()

    # Make sure the activity exists
    if activity is None:
        return render(request, 'invalid-code.html')

    # Make sure the player hasn't already done the activity today
    user_completed_activity = request.user.player_activities.filter(activity__code=code)
    today = date.today()
    user_completed_activity_today = user_completed_activity.filter(
        completion_time__year=today.year,
        completion_time__month=today.month,
        completion_time__day=today.day
    )

    if user_completed_activity_today.count() > 0:
        return render(request, 'already-completed.html')

    if request.method == "POST":
        # Add the activity completion
        completion = ActivityCompletion(player=request.user, activity=activity)
        completion.save()

        new_achievements = check_for_new_achievements(request.user)
        print("Completed new achievements: ", new_achievements)

        response = redirect('completed', activity_id=activity.id)
        if len(new_achievements) > 0:
            query_string = urlencode({"achievements": [ach.id for ach in new_achievements]})
            response['Location'] += '?' + query_string

        # Add new achievements that the player just earned
        for achievement in new_achievements:
            completion = AchievementCompletion(achievement=achievement, player=request.user)
            completion.save()

        return response

    return render(request, 'activity.html', {'activity': activity})


@login_required()
def completed(request, activity_id):
    activity = Activity.objects.filter(id=activity_id).first()

    ctx = {'activity': activity}
    if "achievements" in request.GET:
        achievements_completed_str = request.GET["achievements"]
        achievement_ids = [int(id) for id in achievements_completed_str.strip('][').split(',')]
        achievements_completed = Achievement.objects.filter(id__in=achievement_ids).all()
        ctx['achievements'] = achievements_completed

    return render(request, 'completed.html', ctx)


def achievements(request):
    ctx = {'achievements': Achievement.objects.all()}

    if request.user.is_authenticated:
        ctx['user_achievements'] = request.user.player_achievements.all()

    return render(request, 'achievements.html', ctx)


# How many scores go on each page.
SCORES_PER_PAGE = 20


def leaderboard(request):
    scores = PlayerScore.objects.order_by('-score')

    scores_page = scores[:SCORES_PER_PAGE]

    annotated_scores = [{'rank': idx + 1, 'data': score} for idx, score in enumerate(scores_page)]

    ctx = {'scores': annotated_scores, 'separate_player_score': False}
    if request.user.is_authenticated:
        scores_list = list(scores)
        if request.user.playerscore not in scores_list:
            ctx['separate_player_score'] = True
            ctx['player_score'] = request.user.playerscore.score

            ctx['player_rank'] = scores_list.index(request.user.playerscore) + 1

    return render(request, 'leaderboard.html', ctx)


def map_view(request):
    available_activities = Activity.objects.filter(is_available=True)
    return render(request, 'map-only.html', {'activities': available_activities})


def four_oh_four(request):
    return render(request, '404.html')
