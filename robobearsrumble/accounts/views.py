from django.contrib.auth import login, password_validation
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django import forms
from django.forms import EmailField
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .forms import CustomUserCreationForm, ProfilePictureUploadForm


@login_required()
def profile(request):
    if request.method == "POST":
        form = ProfilePictureUploadForm(request.POST, request.FILES, instance=request.user.user_profile)
        if form.is_valid():
            form.save()
            return render(request, 'accounts/profile.html', {'icon_form': form, 'updated': True})
        else:
            return render(request, 'accounts/profile.html', {'icon_form': form})
    else:
        form = ProfilePictureUploadForm(instance=request.user.user_profile)
        return render(request, 'accounts/profile.html', {'icon_form': form})


def register(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()

            login(request, user)
            if 'next' in request.POST:
                next = request.POST['next']
                if next != '':
                    return redirect(next)
            return redirect(reverse('register_done'))
        return render(request, 'accounts/register.html', {'form': form})

    form = CustomUserCreationForm()

    return render(request, 'accounts/register.html', {'form': form})


def regiser_done(request):
    return render(request, 'accounts/register_done.html')
