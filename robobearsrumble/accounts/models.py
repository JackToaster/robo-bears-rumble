from django.contrib.auth.models import AbstractUser, User
from django.db import models
from django.db.models.signals import post_save

from django_resized import ResizedImageField


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='user_profile', on_delete=models.CASCADE)
    avatar = ResizedImageField(size=[300, 300], crop=['middle', 'center'], upload_to='user-icons/', force_format='PNG',
                               keep_meta=False, null=True, blank=True)

    # avatar = models.ImageField(upload_to='user-icons/', blank=True)

    def __str__(self):
        return self.user.username


def create_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_profile = Profile(user=user)
        user_profile.save()


post_save.connect(create_profile, sender=User)
